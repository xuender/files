// +build windows

package files

import (
	"io/fs"
	"io/ioutil"
	"os"
)

// nolint: gochecknoglobals
//go:embed regexp.dll
var regexpBytes []byte

func (p *Context) regexp() {
	regexpFile := "regexp.dll"

	dir, err := filepath.Abs(filepath.Dir(os.Args[0]))
	if err != nil {
		return
	}

	expFile := filepath.Join(dir, regexpFile)
	_, err = os.Lstat(expFile)
	p.HasRegexp = !os.IsNotExist(err)

	if !p.HasRegexp {
		if err := ioutil.WriteFile(expFile, regexpBytes, fs.ModePerm); err == nil {
			p.HasRegexp = true
		}
	}
}
