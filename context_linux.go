// +buid linux

package files

import (
	_ "embed"

	"io/fs"
	"io/ioutil"
	"os"
	"path/filepath"
)

// nolint: gochecknoglobals
//go:embed regexp.so
var regexpBytes []byte

func (p *Context) regexp() {
	regexpFile := "regexp.so"

	dir, err := filepath.Abs(filepath.Dir(os.Args[0]))
	if err != nil {
		return
	}

	expFile := filepath.Join(dir, regexpFile)
	_, err = os.Lstat(expFile)
	p.HasRegexp = !os.IsNotExist(err)

	if !p.HasRegexp {
		if err := ioutil.WriteFile(expFile, regexpBytes, fs.ModePerm); err == nil {
			p.HasRegexp = true
		}
	}
}
