package files

import "time"

// Config 配置.
type Config struct {
	ID        uint `gorm:"primarykey"`
	CreatedAt time.Time
	Type      int
	Value     string
}
