module gitee.com/xuender/files

go 1.16

require (
	gitee.com/xuender/oils v1.0.48
	github.com/google/wire v0.5.0
	github.com/mattn/go-sqlite3 v1.14.7
	github.com/mitchellh/go-homedir v1.1.0
	github.com/spf13/cobra v1.1.3
	github.com/spf13/viper v1.7.1
	github.com/stretchr/testify v1.7.0
	gorm.io/driver/sqlite v1.1.4
	gorm.io/gorm v1.21.10
)
