package files_test

import (
	"testing"

	"gitee.com/xuender/files"
	"github.com/stretchr/testify/assert"
)

func TestService_Ignore(t *testing.T) {
	t.Parallel()

	s := files.NewService(&files.Context{}, files.NewTestDB())

	assert.Equal(t, 3, len(s.Ignores()))

	s.Ignore("/t")
	s.Ignore("/t")

	assert.Equal(t, 4, len(s.Ignores()))
	assert.Contains(t, s.Ignores(), "/t")

	s.RemoveIgnore("/t")

	assert.Equal(t, 3, len(s.Ignores()))
}

func TestService_Add(t *testing.T) {
	t.Parallel()

	s := files.NewService(&files.Context{}, files.NewTestDB())

	assert.Equal(t, 0, len(s.Dirs()))

	s.Add("/tmp/a")
	s.Add("/tmp/a")
	s.Add("/tmp/a/b")
	s.Add("/tmp")

	assert.Equal(t, 1, len(s.Dirs()))
	assert.Contains(t, s.Dirs(), "/tmp")

	s.RemoveDir("/tmp")

	assert.Equal(t, 0, len(s.Dirs()))
}

func TestService_Index(t *testing.T) {
	t.Parallel()

	s := files.NewService(&files.Context{Limit: 10, Order: "path"}, files.NewTestDB())

	s.Add("cmd")
	s.Ignore("main.go")
	s.Index()

	fs := s.Search([]string{"main"})

	assert.GreaterOrEqual(t, len(fs), 1)

	fs = s.Search([]string{})

	assert.GreaterOrEqual(t, len(fs), 1)
}

// nolint: paralleltest
func TestService_Count(t *testing.T) {
	db := files.NewTestDB()
	ctx := &files.Context{}
	s := files.NewService(ctx, db)
	db.Save(&files.File{
		Path: "aa.jpg",
		Base: "aa.jpg",
		Ext:  ".jpg",
	})
	db.Save(&files.File{
		Path: "d",
		Base: "d",
	})
	db.Save(&files.File{
		Path: "aa.gif",
		Base: "aa.gif",
		Ext:  ".gif",
	})

	assert.Equal(t, int64(0), s.Count([]string{}))
	assert.Equal(t, int64(2), s.Count([]string{"aa"}))
	assert.Equal(t, int64(1), s.Count([]string{"aa", "^gif"}))
	assert.Equal(t, int64(1), s.Count([]string{"aa", "jpg"}))

	ctx.Ext = ".gif"

	assert.Equal(t, int64(1), s.Count([]string{"aa"}))
}
