//+build wireinject

package files

import (
	"github.com/google/wire"
)

// InitApp 初始化应用.
func InitApp(ctx *Context) *App {
	panic(wire.Build(
		NewApp,
		NewService,
		NewDB,
	))
}
