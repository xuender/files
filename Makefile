lint:
	golangci-lint run
	# node_modules/.bin/ng lint
proto:
	protoc --go_out=plugins=grpc:. pb/*.proto
build:
	# node_modules/.bin/ionic build --prod --engine=browser --platform=ios
	go build -o dist/files cmd/main/main.go
	# CGO_ENABLED=0 GOOS=windows GOARCH=amd64 go build -ldflags="-H windowsgui" -o dist/se.exe cmd/se/main.go
windows:
	go build -o dist/files.exe cmd/main/main.go
clean:
	rm -rf www
