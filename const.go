package files

import (
	"fmt"
	"os"
)

const (
	// ByteDir 目录.
	ByteDir = iota
	// ByteFile 文件.
	ByteFile
	// ByteConfig 配置.
	ByteConfig
	// ByteSeq 序列.
	ByteSeq
	// ByteStack 堆栈.
	ByteStack
	// ByteIgnore 忽略.
	ByteIgnore
)

// nolint: gochecknoglobals
var ignoreDirs = []string{"node_modules", "target", fmt.Sprintf("%c.", os.PathSeparator)}
