package main

import (
	_ "embed"

	"gitee.com/xuender/files/cmd"
)

func main() {
	cmd.Execute()
}
