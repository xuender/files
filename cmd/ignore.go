package cmd

import (
	"gitee.com/xuender/files"
	"github.com/spf13/cobra"
)

// nolint: gochecknoinits
func init() {
	ignoreCmd := &cobra.Command{
		Use:     "ignore",
		Aliases: []string{"i"},
		Short:   "忽略",
		Long:    `增加忽略目录或文件名`,
		Run: func(cmd *cobra.Command, args []string) {
			ctx := files.NewContext(cmd)
			app := files.InitApp(ctx)

			app.Ignore(args)
		},
	}

	ignoreCmd.Flags().BoolP("remove", "r", false, "删除忽略目录")
	rootCmd.AddCommand(ignoreCmd)
}
