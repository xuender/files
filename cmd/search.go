package cmd

import (
	"gitee.com/xuender/files"
	"github.com/spf13/cobra"
)

// nolint: gochecknoinits
func init() {
	searchCmd := &cobra.Command{
		Use:     "search",
		Aliases: []string{"s"},
		Short:   "文件搜索",
		Long: `文件搜索
排除关键字使用^开头
例如: files s go ^sum
查找文件命中包含go但不包含sum的文件

统配使用*、%代表任意字符,?代表单个字符
例如: files s ??.sum
可查找 go.sum`,
		Run: func(cmd *cobra.Command, args []string) {
			ctx := files.NewContext(cmd)
			app := files.InitApp(ctx)

			app.Search(args)
		},
	}

	searchCmd.Flags().Int("limit", 100, "显示条数")
	searchCmd.Flags().Int("offset", 0, "起始条数")
	searchCmd.Flags().StringP("ext", "e", "", "扩展文件名")
	searchCmd.Flags().BoolP("simple", "s", false, "简单输出")
	searchCmd.Flags().StringP("order", "o", "path", "排序(选项: path, base, ext, dir, size, mod_time)")
	searchCmd.Flags().Bool("desc", false, "倒序")
	searchCmd.Flags().BoolP("regexp", "r", false, "正则表达式")
	rootCmd.AddCommand(searchCmd)
}
