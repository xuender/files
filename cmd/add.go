package cmd

import (
	"gitee.com/xuender/files"
	"github.com/spf13/cobra"
)

// nolint: gochecknoinits
func init() {
	addCmd := &cobra.Command{
		Use:     "add",
		Aliases: []string{"a"},
		Short:   "增加目录",
		Long:    `增加目录，遍历目录下所有文件、子目录添加入索引`,
		Run: func(cmd *cobra.Command, args []string) {
			ctx := files.NewContext(cmd)
			app := files.InitApp(ctx)

			app.Add(args)
		},
	}

	addCmd.Flags().BoolP("remove", "r", false, "删除目录")
	rootCmd.AddCommand(addCmd)
}
