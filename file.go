package files

import "time"

// File 文件.
type File struct {
	Path    string `gorm:"primarykey"`
	ModTime time.Time
	Base    string
	Ext     string
	Size    int64
	Dir     bool
	Ver     int
}
